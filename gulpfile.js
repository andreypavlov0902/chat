'use strict';
const gulp          = require('gulp');
const sass          = require('gulp-sass');
const sourcemaps    = require('gulp-sourcemaps');
const debug         = require('gulp-debug');
const del           = require('del');
const newer         = require('gulp-newer');
const gulpIf        = require('gulp-if');

const remember     = require('gulp-remember');
const cached       = require('gulp-cached');
const path         = require('path');
const autoprefixer = require('gulp-autoprefixer');
const minifyCss    = require('gulp-minify-css');
const browserSync  = require('browser-sync');
const notify       = require('gulp-notify');
const plumber      = require('gulp-plumber');


gulp.task('prefixer', function () {
    return gulp.src('public/**/*.css')
        .pipe(autoprefixer())
        .pipe(minifyCss())
        .pipe(gulp.dest('public'))
});


const isDevelopment = !process.env.NODE_ENV ||  process.env.NODE_ENV == 'development';

gulp.task('styles', function () {
return gulp.src('sass/**/style.scss')
    .pipe(plumber({
        errorHandler: notify.onError(function (err) {
            return{
                message: err.message
            };
        })
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(cached('styles'))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(remember('styles'))
    .pipe(minifyCss())
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('assets/css'))
});



gulp.task('clean', function () {
    return del('public');
});

gulp.task('test', function () {
    return gulp.src('js/**/*.*', {since: gulp.lastRun('test')})
        .pipe(newer('public/js'))
        .pipe(gulp.dest('public/js'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('styles')));


gulp.task('watch', function () {
    gulp.watch('sass/**/*.*', gulp.series('styles')).on('unlink', function (filepath) {
        console.log(path.basename('sass/style.scss'));
        remember.forget('styles', path.basename(filepath));
        delete cached.caches.styles[path.basename(filepath)];

    });
});

gulp.task('dev', gulp.series('build', 'watch'));

gulp.task('server',function(){
    browserSync.init({
        server:'./'
    });
    browserSync.watch('./**/*.*').on('change', browserSync.reload);
});






















/*
gulp.task('sass:watch', function () {
    gulp.watch('./sass/!**!/!*.scss', ['sass']);
});

gulp.task('minCss',function () {
    return gulp.src('css/!**!/!*.css')
        .pipe(minifyCss({KeepBreak:true}))
        .pipe(gulp.dest('app/css/'))
});

gulp.task('minJson', function () {
    gulp.src('./!**!/!*.json')
        .pipe(jsonmin())
        .pipe(gulp.dest('./app/'));
});*/
