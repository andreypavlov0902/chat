/**
 * Created by andrei on 13.07.2017.
 */

let path = require('path');
let express = require('express');
let WebSocketServer = require('ws').Server;
let ServerExpress = require('./server.js');


class MyServer {
    constructor(ws){
        this.wss = new ws({port: 8080});
        this.linksConnects = [];
        this.connect(this.wss);
    }
    connect(wss) {
        wss.on('connection', (ws) => {
            this.addToArray(this.linksConnects, ws);
            this.countConnectsSend(ws);
            this.OnMessage(ws);
            this.closeConnect(ws);
        })
    }
    closeConnect(ws) {
        ws.on('close', () => {
            this.deleteInArrayConnectDeprecatedId(ws);
            this.countConnectsSend(ws);
        });
    }
    deleteInArrayConnectDeprecatedId(ws){
        this.linksConnects.splice(this.linksConnects.findIndex((elem) => {
            return elem === ws;
        }), 1);
    }
    OnMessage(ws) {
        ws.on('message', (message) => {
         this.forEachArraySend(this.linksConnects, message);
        });
    }
    forEachArraySend(arr, message) {
        arr.forEach((ws) => {
            if(ws.readyState === 1) {
                ws.send(message);
            }
        })
    }
    addToArray(arr, args) {
        arr.push(args);
    }
    countConnectsSend(ws) {
        this.linksConnects.forEach((ws) => {
            if(ws.readyState === 1) {
                ws.send(JSON.stringify({
                    type: 'countConnect',
                    length: this.linksConnects.length
                }));
            }
        });
    }
}

new MyServer(WebSocketServer);
new ServerExpress(express, path);