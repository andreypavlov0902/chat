/**
 * Created by andrei on 13.07.2017.
 */
((window)=> {
    class Chat {
        constructor(hb) {
            this.button       = this.querySelector('.button');
            this.buttonTemp   = this.querySelector('.template-attach-button').innerHTML;
            this.imgAttach    = this.querySelector('.template-attach-img').innerHTML;
            this.attachBlock  = this.querySelector('.block-file__text');
            this.text         = this.querySelector('.sms');
            this.template     = this.querySelector('.my-template').innerHTML;
            this.inputNick    = this.querySelector('.nic');
            this.fileInput    = this.querySelector('.file')
            this.blockSms     = this.querySelector('.message');
            this.socket       = new WebSocket("ws://localhost:8080");
            this.handlebars   = hb;
            this.yourFile     = null;
            this.img64        = null;
            this.userId       = null;
            this.message      = null;
            this.removeImg    = null;
            this.openConnect(this.socket);
            this.getMessage();
            this.setFile();
            this.sendMessageInclick();
            this.sendMessageInKeyup();
            this.closes();
            this.setAttach();
            this.attach();

        }
        closes() {
            window.onunload = () => {
                this.socket.close(1000, 'close');
            }
        }
        attach() {
            this.attachBlock.addEventListener('click', (e) => {
                e.preventDefault();
                this.fileInput.click();
            })
        }
        getMessage() {
            this.socket.onmessage = (event) => {
                this.message = JSON.parse(event.data)
                switch (this.message.type) {
                    case 'countConnect':
                        this.addConectUserInPage(this.message.length)
                        break;
                    case 'sms':
                        this.addPost(this.message)
                        break;
                }
            }
        }
        send() {
            let nick = (this.inputNick.value === '') ? 'Guest' : this.inputNick.value;
            this.socket.send(JSON.stringify({
                nick,
                type: 'sms',
                text: this.text.value,
                time: new Date(),
                id  : this.userId,
                file: this.img64,

            }));
            this.text.value = '';
            this.fileInput.value = '';
            this.img64 = null;
            this.setDefoultAttach();
        }
        sendMessageInclick() {
            this.button.addEventListener('click', () => {
                if (this.text.value !== '' || this.img64 != null)
                this.send();
            });
        }
        sendMessageInKeyup() {
            this.text.addEventListener('keyup', (e) => {
                if (e.keyCode === 13 && this.text.value !== '' || this.img64 != null )
                this.send();
            });
        }
        setFile() {
            this.fileInput.addEventListener('change', (e) => {

                if (e.target.files[0]) {
                    return  this.transferBase64( e.target.files[0]);
                }
                this.setDefoultAttach();
            });
        }
        setDefoultAttach() {
            this.clearAttach();
            this.setAttach();
        }
        clearAttach() {
            this.attachBlock.innerHTML = '';
        }
        setAttach() {
            let buttonAttach = this.compileTemplate(this.buttonTemp);
            this.attachBlock.innerHTML = buttonAttach();
        }
        preview() {
            this.clearAttach();
            let buttonAttach = this.compileTemplate(this.imgAttach);
            this.attachBlock.innerHTML = buttonAttach({data: this.img64});
            this.removeAttach();
        }
        removeAttach() {
            let buttonI = this.querySelector('.fa-times');
            buttonI.addEventListener('click', (e) => {
                e.preventDefault();
                e.stopPropagation();
                this.setDefoultAttach();
            })
        }
        transferBase64(file) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.img64 = e.target.result;
                this.preview();
            };
            reader.readAsDataURL(file);
        }
        openConnect(socket) {
            socket.onopen = (e) => {
                this.userId = e.timeStamp.toFixed(3);
            };
        }
        querySelector(selector) {
            return document.querySelector(selector);
        }
        addConectUserInPage (count) {
            let span = this.querySelector('.countUser')
            span.innerText = '';
            span.innerText = count;
        }
        addPost (message) {
            let toRenderTemplate,
            html;
            message.nick = (message.id === this.userId && message.nick === 'Guest') ? 'You' : message.nick;
            message.class = (message.id === this.userId) ? 'message-self' : 'message-other';
            message.time = new Date(message.time).toLocaleTimeString();
            toRenderTemplate = this.compileTemplate(this.template);
            html = toRenderTemplate(message);
            this.blockSms.innerHTML += html;
        }
        compileTemplate(template) {
            return this.handlebars.compile(template);
        }
    }
    new Chat(Handlebars)

})(window)
