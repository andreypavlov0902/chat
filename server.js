/**
 * Created by andrei on 13.07.2017.
 */
/*
let express = require('express');
let path = require('path');
let app = express();
app.use(express.static(__dirname +'/assets/'));
app.use(express.static(__dirname +'/fonts'));



app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.listen(3001);
*/

module.exports = class ServerExpress {
    constructor(express, path) {
        this.path = path;
        this.express = express;
        this.app = this.express();
        this.folderByDefault('/assets/');
        this.getFile();
        this.listener(3001);
    }
    folderByDefault (folder) {
        this.app.use(this.express.static(__dirname + folder));
    }
    getFile() {
        this.app.get('/',  (req, res) => {
            res.sendFile(this.path.join(__dirname + '/index.html'));
        });
    }
    listener(port) {
        this.app.listen(port);
    }
};

